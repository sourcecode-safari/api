package main

import (
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func main() {

	V1api := gin.Default()
	V1api.Use(cors.Default())

	js := V1api.Group("v1/jokestore")
	{
		// Actions
		js.GET("/", joke)
		js.GET("/list", jokeList)
		js.GET("/help", help)
	}

	V1api.GET("/ping", pong)
	V1api.Run(":8090")
}

func pong(c *gin.Context) {
	c.JSON(http.StatusOK, "pong")
}
