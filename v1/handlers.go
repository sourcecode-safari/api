package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

//********************
//* JOKESTORE ROUTES *
//********************

// R => /v1/jokestore/?key=:key
func joke(c *gin.Context) {
	key := c.DefaultQuery("key", jokeStore.rnd())
	c.JSON(http.StatusOK, gin.H{
		"key":  key,
		"joke": jokeStore[key],
	})
}

// R => "/v1/jokestore/list"
func jokeList(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"Keylist": jokeStore.keys(),
	})
}

// R => "/v1/jokestore/help"
func help(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"/v1/jokestore/":     "returns a random joke or a specifc one with ?key=:key",
		"/v1/jokestore/list": "returns a list of all keys in the jokestore",
		"/v1/jokestore/help": "returns a list of all routes available from the jokestore api",
	})
}
