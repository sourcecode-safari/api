package main

import "math/rand"

// KeyValStore is our mock data structure.

type keyValStore map[string]string

func (kvs keyValStore) keys() []string {
	ks := []string{}
	for k := range kvs {
		ks = append(ks, k)
	}
	return ks
}

func (kvs keyValStore) rnd() string {
	ks := kvs.keys()
	return ks[rand.Intn(len(kvs))]
}

// JokeStore is the coresponding mock data.
var jokeStore = keyValStore{
	"bike":        "A bike cant stand alone, because its two tired.",
	"shifts":      "I just got fired from my job at the keyboard factory. They told me I wasn't putting in enough shifts.",
	"autocorrect": "We'll we'll we'll...if it isn't autocorrect.",
}
